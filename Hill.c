#include <stdio.h>
#include <stdlib.h>
#include "Matrice.h"
#include <string.h>
#include "Arithm�tique.h"
#include <time.h>

/** This algorithms works only for letters in caps for example : "ABCDEF*. It won't work for "abcdef" */
int char_key(char c){
    /** Transform a char into a integer and return the value minus 65 */
    int a = c;
    return c-65;
}

char key_char(int new_c_val){
    /** Transform a integer into a char and return the char plus 65 */
    return new_c_val+65;
}

Matrice* HillMatriceGen(int taille){
    srand(time(NULL));
    Matrice* m;
    m = remplissageRamdomiseMat(taille);
    while(pgcd(determ(m->mat, taille), 26) != 1){
        libererMatrice(m);
        m = remplissageRamdomiseMat(taille);
    }
    return m;
}

void encrypt(FILE* file_to_encrypt,char* encrypt_content_file,int N, int blocs){
    /**Encryption function, take a file and encrypt the content into a new file */
    /* Opening a new file that will contain the encrypted informations */
    FILE *encrypted_file = fopen(encrypt_content_file,"w");
    if(encrypted_file==NULL){
        printf("ERROR FILE\n");
        return;
    }
    /* On initialise une matrice de hill de taille "blocs x blocs" */
    Matrice *Hillmat = HillMatriceGen(blocs);
    //afficheMatrice(Hillmat);
    /* INIT OTHER VAR */
    char ligne[N];
    char* L = ligne;
    int i = 0;
    char cz[blocs];
    int elem;
    int length_line = 0;
    /* On initialise une nouvelle matrice avec une taille "blocs x 1"*/
    Matrice *a = newMatrice(blocs,1,0);
    Matrice *poub;
    /* FOR EACH LINE IN THE FILE WE WILL ENCODE EACH TUPLE OF LETTERS */
    while(fgets(L,N,file_to_encrypt)){
        length_line = strlen(L);

        while(i < length_line-1){

            /* IF WE TRY TO ENCRYPT A CHAR THAT ISN T RECOGNIZED SUBSTITUTE IT BY AN 'X'
            WORK EITHER IF THE NUMBER OF LETTER ISNT EVEN */
            for(int j = 0; (j<length_line)&&(j<blocs);j++){
                elem = sscanf(L,"%c",&cz[j]);
                printf("%c\n",cz[j]);
                if((char_key(cz[j])<0)||(char_key(cz[j])>26)){
                    cz[j] = 'X';
                }
                a->mat[j][0] = char_key(cz[j]);
                L++;
                i++;
            }
            afficheMatrice(a);
            poub = a;
            a = produitMatrices(Hillmat,a);
            libererMatrice(poub);
            poub=a;
            a = moduloOperation(a,26);
            libererMatrice(poub);
            for(int j=0;j<blocs;j++){
                cz[j] = key_char(a->mat[j][0]);
                fprintf(encrypted_file,"%c",cz[j]);
            }

        }
        fprintf(encrypted_file,"\n");
        i = 0;
        L = ligne;
    }
    printf("Your file content is encrypted in : %s file",encrypt_content_file);
    fclose(encrypted_file);
    libererMatrice(a);
    libererMatrice(Hillmat);
}
