int pgcd(int a, int b){
        int reste = 0;
        int entier_c1 = a;
        int entier_c2 = b;
        while(b!=0){
            reste = a%b;
            a = b;
            b = reste;
        }
        return a;
}
