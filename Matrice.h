#ifndef __MATRICE__
#define __MATRICE__
typedef struct{
    int colonnes;
    int lignes;
    int **mat;
} Matrice;
void afficheMatrice();
Matrice* newMatrice(int lignes,int colonnes,int init);
Matrice* additionMatrices(Matrice* a, Matrice* b);
Matrice* produitMatrices(Matrice *a, Matrice *b);
Matrice *moduloOperation(Matrice* a,int mod_val);
void libererMatrice(Matrice *a);
void libererMatriceList(Matrice **a,int tailleList);
int determ(int **m,int n);
Matrice* remplissageRamdomiseMat(int taille);
#endif // __MATRICE__
