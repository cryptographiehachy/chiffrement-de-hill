#include <stdio.h>
#include <stdlib.h>
#include "Matrice.h"
#include <time.h>
#define RAND_MAX 100
#include <math.h>



Matrice* newMatrice(int lignes,int colonnes,int init){
    Matrice* a = (Matrice*)malloc(sizeof(Matrice));
    if(a==NULL){
        printf("Erreur d'allocation a");
        return NULL;
    }
    a->colonnes = colonnes;
    a->lignes = lignes;
    a->mat = (int**)malloc(sizeof(int*)*lignes);
    // Erreur
    if(a->mat==NULL){
        free(a); // Malloc matrice a
        printf("Erreur d'allocation a->mat");
        return NULL;
    }
    for(int i=0;i<lignes;i++){
        a->mat[i]=(int*)malloc(sizeof(int)*colonnes);
        if(a->mat[i]==NULL){
            for(int w=i-1;w>=0;w--){
                free(a->mat[w]);
            }
            free(a->mat);
            free(a);
            return NULL;
        }
        for(int j=0; j< colonnes; j++){
            a->mat[i][j] = init;
        }
    }
    return a;
}

Matrice* remplissageRamdomiseMat(int taille){
    Matrice *m = newMatrice(taille,taille,0);
    for(int i=0; i<m->lignes; i++){
        for(int j=0; j<m->colonnes; j++){
            m->mat[i][j] = rand()%1001;
        }
    }
    //afficheMatrice(m);
    return m;
}

void afficheMatrice(Matrice* a){
    printf("Affichage Matrice\n");
    for(int i=0;i<a->lignes;i++){
        for(int j=0; j< a->colonnes; j++){
            printf(" %d ",a->mat[i][j]);
        }
        printf("\n");
    }
}

Matrice* additionMatrices(Matrice* a, Matrice* b){
    /* On suppose que ce sont des matrices de m�mes taille */
    Matrice *c = newMatrice(a->lignes,a->colonnes,0);
    for(int i=0;i<a->lignes;i++){
        for(int j=0; j< a->colonnes; j++){
            c->mat[i][j] = a->mat[i][j] + b->mat[i][j];
        }
    }
    return c;
}

Matrice* produitMatrices(Matrice *a, Matrice *b){
    /* On suppose que A : x croix y et B : y croix w */
    Matrice *c = newMatrice(a->lignes,b->colonnes,0);
    for(int i = 0; i < b->colonnes;i++){
        for(int j=0; j<a->lignes;j++){
            for(int w=0; w<a->colonnes;w++){
                c->mat[j][i] += a->mat[j][w] * b->mat[w][i];
            }
        }
    }
    return c;
}

Matrice *moduloOperation(Matrice* a,int mod_val){
     Matrice *c = newMatrice(a->lignes,a->colonnes,0);
     for(int i=0;i<a->lignes;i++){
        for(int j=0; j< a->colonnes; j++){
            c->mat[i][j] = a->mat[i][j] % mod_val;
            if(c->mat[i][j] < 0){
                c->mat[i][j] = c->mat[i][j] * -1;
            }
        }
    }
    return c;
}

Matrice* inverseMatrice(Matrice *a){
    /* On suppose que la matrice a est inversible */
    Matrice *c = newMatrice(a->lignes,a->colonnes,0);
}


void libererMatriceList(Matrice **a,int tailleList){
   for(int i = 0; i < tailleList; i++){
        libererMatrice(a[i]);
   }
}

void libererMatrice(Matrice *a){
    for(int i = 0; i < a->lignes;i++){
        free((a->mat[i]));
    }
    free(a->mat);
    free(a);
}

int determ(int **m,int n) {
    int det=0, p, h, k, i, j;
    Matrice *temp = newMatrice(n-1,n-1,0);
    if(n==1) {
        return m[0][0];
    }

    if(n==2) {
        det=(m[0][0]*m[1][1]-m[0][1]*m[1][0]);
        return det;
    }

    else{
        for(p=0; p<n; p++){
            h = 0;
            k = 0;
            for(i=1; i<n; i++){
                for(j=0; j<n; j++){
                    if(j==p){
                        continue;
                    }
                    temp->mat[h][k] = m[i][j];
                    k++;
                    if(k == n-1){
                        h++;
                        k=0;
                    }
                }
            }

            det = det + m[0][p] * pow(-1,p)* determ(temp->mat, n-1);
        }
        libererMatrice(temp);
        return det;
    }
}



/* {
    for(p=0;p<n;p++) {
      h = 0;
      k = 0;
      for(i=1;i<n;i++) {
        for( j=0;j<n;j++) {
          if(j==p) {
            continue;
          }
          temp[h][k] = m[i][j];
          k++;
          if(k==n-1) {
            h++;
            k = 0;
          }
        }
      }
      det=det+m[0][p]*pow(-1,p)*determ(temp,n-1);
    }
    return det;
  }
  */
